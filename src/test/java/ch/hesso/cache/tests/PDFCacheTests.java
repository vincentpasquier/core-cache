/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.tests;

import ch.hesso.cache.PDFCache;
import ch.hesso.predict.restful.PDFFile;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PDFCacheTests {


	@Test
	public void testGenerateTables () throws IOException, CacheQueryException {
		PDFCache.Builder builder = PDFCache.Builder.newBuilder ( new File ( "/mnt/Damocles/database" ) );
		PDFCache cache = builder.partitions ( 1 ).build ();
		String url = "http://www.cs.mcgill.ca/~colt2009/papers/037.pdf#page=1";
		PDFFile pdf = new PDFFile ();
		pdf.setUrl ( url );
		String content = "Some text ... Vox Populi: Collecting High-Quality Labels from a Crowd";
		String abs = "With the emergence of search engines and crowd-" +
				"sourcing websites, machine learning practitioners\n" +
				"are faced with datasets that are labeled by a large\n" +
				"heterogeneous set of teachers. These datasets test\n" +
				"the limits of our existing learning theory, which\n" +
				"largely assumes that data is sampled i.i.d. from a\n" +
				"fixed distribution. In many cases, the number of\n" +
				"teachers actually scales with the number of exam-\n" +
				"ples, with each teacher providing just a handful of\n" +
				"labels, precluding any statistically reliable assess-\n" +
				"ment of an individual teacher’s quality. In this pa-\n" +
				"per, we study the problem of pruning low-quality\n" +
				"teachers in a crowd, in order to improve the la-\n" +
				"bel quality of our training set. Despite the hur-\n" +
				"dles mentioned above, we show that this is in fact\n" +
				"achievable with a simple and efficient algorithm,\n" +
				"which does not require that each example be re-\n" +
				"peatedly labeled by multiple teachers. We provide\n" +
				"a theoretical analysis of our algorithm and back\n" +
				"our findings with empirical evidence.";
		pdf.setContent ( content );
		pdf.setAbsContent ( abs );

		//cache.cache ( pdf );

		CacheQuery<PDFFile> query = cache.newQuery ( url ).execute ();
		Assert.assertTrue ( query.hasMore () );
		PDFFile stored = query.next ();
		System.out.println (stored.getContent ());
		System.out.println (stored.getUrl ());
		System.out.println (stored.getAbsContent ());
		Assert.assertTrue ( stored.getUrl ().equals ( url ) );
		Assert.assertTrue ( stored.getContent ().equals ( content ) );
		Assert.assertTrue ( stored.getAbsContent ().equals ( abs ) );
	}


}
