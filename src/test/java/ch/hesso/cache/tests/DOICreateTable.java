/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.tests;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DOICreateTable {

	//
	public static final String TABLE_NAME = "DOI";

	//
	private static final String TABLE_SCHEMA = "CREATE TABLE " + TABLE_NAME + " ( " +
			"doi text primary key, location text);";

	//
	private static final String TABLE_INSERT = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?);";

	@Test
	public void createTable () throws ClassNotFoundException, SQLException {
		String path = "/mnt/Damocles/master/doi/DOI_0.sqlite3";

		Class.forName ( "org.sqlite.JDBC" );
		Connection connection = DriverManager.getConnection ( "jdbc:sqlite:" + path );
		Statement st = connection.createStatement ();
		st.execute ( TABLE_SCHEMA );
		st.close ();
	}
}
