/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.tests;

import ch.hesso.cache.DOICache;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.Doi;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DOICacheTests {

	@Test
	public void testGenerateTables () throws IOException, CacheQueryException {
		DOICache.Builder builder = DOICache.Builder.newBuilder ( new File ( "/mnt/Damocles/master/doi" ) );
		DOICache cache = builder.partitions ( 1 ).build ();
		Doi doi = new Doi ();
		doi.setDoi ( "http://dx.doi.org/10.1109/SASO.2008.25" );
		doi.setForward ( "http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=4663419" );

		Doi doi2 = new Doi ();
		doi2.setDoi ( "http://dx.doi.org/10.1109/SASO.2008.215" );
		doi2.setForward ( "http://ieeexplore.ieee.org/lpdocs/ep1ic03/wrapper.htm?arnumber=4663419" );

		Doi doi3 = new Doi ();
		doi3.setDoi ( "http://dx.1doi.org/10.1109/SASO.2008.215" );
		doi3.setForward ( "http://ieeexplore.ieee.org/lpdocs/ep1ic03/wrapper.htm?arnumber=4663419" );

		Doi doi4 = new Doi ();
		doi4.setDoi ( "http://dx.doi.1org/10.1109/SASO.2008.215" );
		doi4.setForward ( "http://ieeexplore.ieee.org/lpdocs/ep1ic03/wrapper.htm?arnumber=4663419" );

		cache.cache ( doi );
		cache.cache ( doi2 );
		cache.cache ( doi3 );
		cache.cache ( doi4 );

		CacheQuery<Doi> query = cache.newQuery ( doi.getDoi () ).execute ();
		Assert.assertTrue ( query.hasMore () );
		Doi stored = query.next ();
		Assert.assertTrue ( doi.getDoi ().equals ( stored.getDoi () ) );
		Assert.assertTrue ( doi.getForward ().equals ( stored.getForward () ) );

		CacheQuery<Doi> query1 = cache.newQuery ( doi2.getDoi () ).execute ();
		Assert.assertTrue ( query1.hasMore () );
		Doi stored1 = query1.next ();
		Assert.assertTrue ( doi2.getDoi ().equals ( stored1.getDoi () ) );
		Assert.assertTrue ( doi2.getForward ().equals ( stored1.getForward () ) );

		CacheQuery<Doi> query2 = cache.newQuery ( doi3.getDoi () ).execute ();
		Assert.assertTrue ( query2.hasMore () );
		Doi stored2 = query2.next ();
		Assert.assertTrue ( doi3.getDoi ().equals ( stored2.getDoi () ) );
		Assert.assertTrue ( doi3.getForward ().equals ( stored2.getForward () ) );

		CacheQuery<Doi> query3 = cache.newQuery ( doi4.getDoi () ).execute ();
		Assert.assertTrue ( query3.hasMore () );
		Doi stored3 = query3.next ();
		Assert.assertTrue ( doi4.getDoi ().equals ( stored3.getDoi () ) );
		Assert.assertTrue ( doi4.getForward ().equals ( stored3.getForward () ) );
	}

}
