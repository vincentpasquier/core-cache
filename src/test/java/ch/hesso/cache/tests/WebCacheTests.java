/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.tests;

import ch.hesso.cache.WebCache;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.WebPage;
import com.google.common.base.Joiner;
import com.google.common.io.Files;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WebCacheTests {

	@Test
	public void testGenerateTables () throws IOException, CacheQueryException {
		WebCache.Builder builder = WebCache.Builder.newBuilder ( new File ( "/mnt/Damocles/database" ) );
		WebCache cache = builder.partitions ( 1 ).build ();
		String url = "http://dl.acm.org/citation.cfm?id=2207980";
		WebPage webPage = new WebPage ();
		webPage.setUrl ( url );
		File f = new File ( WebCacheTests.class.getResource ( "/test.html" ).getFile () );
		Assert.assertTrue ( f.exists () );
		List<String> lines = Files.readLines ( f, Charset.defaultCharset () );
		String content = Joiner.on ( "" ).join ( lines );
		webPage.setContent ( content );
		//cache.cache ( page );

		CacheQuery<WebPage> query = cache.newQuery ( url ).execute ();
		Assert.assertTrue ( query.hasMore () );
		WebPage web = query.next ();
		Assert.assertTrue ( web.getUrl ().equals ( url ) );
		Assert.assertTrue ( web.getContent ().equals ( content ) );
	}

}
