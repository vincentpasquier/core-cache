/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.doi;

import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.Doi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DOICacheQueryImpl implements CacheQuery<Doi> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( DOICacheQueryImpl.class );

	private final ResultSet _rs;

	public DOICacheQueryImpl ( final ResultSet rs ) {
		_rs = rs;
	}

	@Override
	public Doi next () {
		try {
			//_rs.next ();
			String url = _rs.getString ( 1 );
			String location = _rs.getString ( 2 );

			Doi doi = new Doi ();
			doi.setDoi ( url );
			doi.setForward ( location );
			return doi;
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while loading Doi", e );
		}
		return null;
	}

	@Override
	public boolean hasMore () {
		try {
			return _rs.next ();
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while looking for more Doi", e );
		}
		return false;
	}

	@Override
	public void close () throws IOException {
		try {
			_rs.close ();
		} catch ( SQLException e ) {
			LOG.debug ( "Error while closing result set", e );
		}
	}
}
