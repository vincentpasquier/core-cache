/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.doi;

import ch.hesso.cache.DOICache;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.manager.SQLitePartitionManager;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.cache.query.CacheQueryBuilder;
import ch.hesso.predict.restful.Doi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DOICacheQueryBuilderImpl implements CacheQueryBuilder<Doi> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( DOICacheQueryBuilderImpl.class );

	//
	private final StringBuilder _sb;

	//
	private Connection _connection;

	//
	private int balance = 0;

	public DOICacheQueryBuilderImpl ( final String id, final Connection connection ) throws SQLException {
		_connection = connection;
		_sb = new StringBuilder ( String.format ( CacheQueryBuilder.TABLE_SELECT, DOICache.TABLE_NAME ) );
		is ( "doi", id );
	}

	/**
	 * @return
	 */
	protected Connection connection () {
		return _connection;
	}

	@Override
	public CacheQueryBuilder<Doi> and () {
		_sb.append ( "and " );
		return this;
	}

	@Override
	public CacheQueryBuilder<Doi> or () {
		_sb.append ( "or " );
		return this;
	}

	@Override
	public CacheQueryBuilder<Doi> openGroup () {
		balance++;
		_sb.append ( "( " );
		return this;
	}

	@Override
	public CacheQueryBuilder<Doi> closeGroup () {
		balance--;
		_sb.append ( ") " );
		return this;
	}

	@Override
	public CacheQueryBuilder<Doi> is ( final String field, final String string ) {
		_sb.append ( field ).append ( " = '" ).append ( string ).append ( "' " );
		return this;
	}

	@Override
	public CacheQueryBuilder<Doi> is ( final String field, final int value ) {
		_sb.append ( field ).append ( " = " ).append ( value ).append ( " " );
		return this;
	}

	@Override
	public CacheQuery<Doi> execute () throws CacheQueryException {
		if ( balance != 0 ) {
			LOG.error ( "One of your group is not balanced, check your expression again." );
			throw new CacheQueryException ( "Query not balanced." );
		}
		ResultSet rs;
		try {
			Statement statement = _connection.createStatement ();
			System.out.println (_sb.toString ());
			statement.execute ( _sb.toString () );
			rs = statement.getResultSet ();

		} catch ( SQLException e ) {
			LOG.error ( e.getMessage () );
			throw new CacheQueryException ( e );
		}
		return new DOICacheQueryImpl ( rs );
	}
}
