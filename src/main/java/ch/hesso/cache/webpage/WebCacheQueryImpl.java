/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.webpage;

import ch.hesso.cache.query.CacheQuery;
import ch.hesso.commons.ObjectManipulation;
import ch.hesso.predict.restful.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WebCacheQueryImpl implements CacheQuery<WebPage> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebCacheQueryImpl.class );

	private final ResultSet _rs;

	public WebCacheQueryImpl ( final ResultSet rs ) {
		_rs = rs;
	}

	@Override
	public WebPage next () {
		try {
			String url = _rs.getString ( 1 );
			String compressedContent = _rs.getString ( 2 );
			String content = ObjectManipulation.decompressString ( compressedContent );
			long time = _rs.getLong ( 3 );
			long ttl = _rs.getLong ( 4 );
			WebPage webPage = new WebPage ();
			webPage.setUrl ( url );
			webPage.setContent ( content );
			webPage.setTime ( time );
			webPage.setTtl ( ttl );

			return webPage;
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while loading WebPage", e );
		}
		return null;
	}

	@Override
	public boolean hasMore () {
		try {
			return _rs.next ();
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while looking for more WebPage", e );
		}
		return false;
	}

	@Override
	public void close () throws IOException {
		try {
			_rs.close ();
		} catch ( SQLException e ) {
			LOG.debug ( "Error while closing result set", e );
		}
	}
}
