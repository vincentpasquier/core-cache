/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.pdf;

import ch.hesso.cache.query.CacheQuery;
import ch.hesso.commons.ObjectManipulation;
import ch.hesso.predict.restful.PDFFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PDFCacheQueryImpl implements CacheQuery<PDFFile> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( PDFCacheQueryImpl.class );

	private final ResultSet _rs;

	public PDFCacheQueryImpl ( final ResultSet rs ) {
		_rs = rs;
	}

	@Override
	public PDFFile next () {
		try {
			//_rs.next ();
			PDFFile pdf = new PDFFile ();
			String url = _rs.getString ( 1 );
			pdf.setUrl ( url );
			String compressedContent = _rs.getString ( 2 );
			if ( compressedContent != null ) {
				String content = ObjectManipulation.decompressString ( compressedContent );
				pdf.setContent ( content );
			}
			String compressedText = _rs.getString ( 3 );
			if ( compressedText != null ) {
				String text = ObjectManipulation.decompressString ( compressedText );
				pdf.setText ( text );
			}
			String compressedAbs = _rs.getString ( 4 );
			if ( compressedAbs != null ) {
				String abs = ObjectManipulation.decompressString ( compressedAbs );
				pdf.setAbsContent ( abs );
			}
			return pdf;
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while loading WebPage", e );
		}
		return null;
	}

	@Override
	public boolean hasMore () {
		try {
			return _rs.next ();
		} catch ( SQLException e ) {
			LOG.debug ( "Exception while looking for more WebPage", e );
		}
		return false;
	}

	@Override
	public void close () throws IOException {
		try {
			_rs.close ();
		} catch ( SQLException e ) {
			LOG.debug ( "Error while closing result set", e );
		}
	}
}
