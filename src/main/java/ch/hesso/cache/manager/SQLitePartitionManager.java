/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class SQLitePartitionManager {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( SQLitePartitionManager.class );

	//
	private final File _directory;

	//
	private final String _table;

	//
	private final Map<Integer, File> _databases;

	//
	private final int _parts;

	private SQLitePartitionManager ( final Builder builder ) {
		_directory = builder._directory;
		_table = builder._table;
		_databases = builder._databases;
		_parts = builder._parts;
	}

	public Connection connection ( final String id ) throws SQLException {
		int part = Math.abs ( id.hashCode () ) % _parts;
		Connection connection = DriverManager.getConnection ( "jdbc:sqlite:" + _databases.get ( part ) );
		return connection;
	}

	/**
	 *
	 */
	public static final class Builder {

		//
		private File _directory;

		//
		private String _table;

		//
		private String _scheme;

		//
		private Map<Integer, File> _databases;

		//
		private int _parts;

		//
		private Builder () {
			_databases = new HashMap<> ();
			_parts = 1;
		}

		/**
		 * @param directory
		 * @param table
		 * @param scheme
		 *
		 * @return
		 */
		public static Builder newBuilder ( final File directory, final String table, final String scheme ) {
			return new Builder ().directory ( directory ).table ( table ).scheme ( scheme );
		}

		/**
		 * @param directory
		 *
		 * @return
		 */
		public Builder directory ( final File directory ) {
			_directory = directory;
			return this;
		}

		/**
		 * @param table
		 *
		 * @return
		 */
		public Builder table ( final String table ) {
			_table = table;
			return this;
		}

		/**
		 * @param scheme
		 *
		 * @return
		 */
		public Builder scheme ( final String scheme ) {
			_scheme = scheme;
			return this;
		}

		/**
		 * @param parts
		 *
		 * @return
		 */
		public Builder partition ( final int parts ) {
			_parts = parts;
			return this;
		}

		/**
		 * Creates each table if does not exists
		 *
		 * @return
		 */
		public SQLitePartitionManager build () {
			// Loads SQLite Driver
			try {
				Class.forName ( "org.sqlite.JDBC" );
			} catch ( ClassNotFoundException e ) {
				LOG.error ( "Error while loading SQLite JDBC", e );
			}

			// Generates files with permutations
			List<Integer> perms = partitionNames ( _parts );
			for ( Integer perm : perms ) {
				File file = new File ( _directory.getAbsolutePath () + "/" + _table + "_" + perm + ".sqlite3" );
				_databases.put ( perm, file );
			}

			// Creates databases if does not exists
			for ( Map.Entry<Integer, File> entry : _databases.entrySet () ) {
				try {
					Connection connection = DriverManager.getConnection ( "jdbc:sqlite:" + entry.getValue ().getAbsolutePath () );
					PreparedStatement pst = connection.prepareStatement ( "select name from sqlite_master WHERE type='table' AND name=?" );
					pst.setString ( 1, _table );
					ResultSet rs = pst.executeQuery ();
					if ( !rs.next () ) {
						Statement st = connection.createStatement ();
						st.execute ( _scheme );
						st.close ();
					}
					rs.close ();
					connection.close ();
				} catch ( SQLException e ) {
					LOG.debug ( "Error while creating permutations databases", e );
				}
			}

			SQLitePartitionManager manager = new SQLitePartitionManager ( this );
			return manager;
		}


		public static List<Integer> partitionNames ( int partition ) {
			List<Integer> perms = new ArrayList<> ( partition );
			for ( int i = 0; i < partition; i++ ) {
				perms.add ( i );
			}
			return perms;
		}

	}

}
