/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.cache;

import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.manager.SQLitePartitionManager;
import ch.hesso.cache.query.CacheQueryBuilder;
import ch.hesso.cache.webpage.WebCacheQueryBuilderImpl;
import ch.hesso.commons.ObjectManipulation;
import ch.hesso.predict.restful.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WebCache implements Cache<WebPage> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebCache.class );

	//
	private final SQLitePartitionManager _manager;

	//
	public static final String TABLE_NAME = "WEBPAGE";

	//
	private static final String TABLE_SCHEMA = "CREATE TABLE " + TABLE_NAME + " ( " +
			"url text primary key, content text, time integer, ttl integer);";

	//
	private static final String TABLE_INSERT = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?);";

	//
	private WebCache ( final Builder builder ) {
		SQLitePartitionManager.Builder sqlBuilder
				= SQLitePartitionManager.Builder.newBuilder ( builder._directory, TABLE_NAME, TABLE_SCHEMA ).partition ( builder._parts );
		_manager = sqlBuilder.build ();
	}

	/**
	 * @param obj
	 *
	 * @return
	 */
	@Override
	public boolean cache ( final WebPage obj ) {
		boolean success = true;
		String urlString = obj.getUrl ();

		try {
			Connection connection = _manager.connection ( urlString );
			PreparedStatement st = connection.prepareStatement ( TABLE_INSERT );
			st.setString ( 1, obj.getUrl () );
			String compressContent = ObjectManipulation.compressString ( obj.getContent () );
			st.setString ( 2, compressContent );
			st.setLong ( 3, obj.getTime () );
			st.setLong ( 4, obj.getTtl () );
			st.execute ();
			connection.close ();
		} catch ( SQLException e ) {
			LOG.debug ( String.format ( "Error while accessing database for %s", urlString ), e );
			success = false;
		}
		return success;
	}

	/**
	 * @param url
	 *
	 * @return
	 *
	 * @throws CacheQueryException
	 */
	public CacheQueryBuilder<WebPage> newQuery ( final String url ) throws CacheQueryException {
		try {
			return new WebCacheQueryBuilderImpl ( url, _manager );
		} catch ( SQLException e ) {
			throw new CacheQueryException ( e );
		}
	}

	/**
	 *
	 */
	public static final class Builder {

		//
		private File _directory;

		//
		private int _parts;

		//
		private Builder () {
			_parts = 1;
		}

		/**
		 * @param directory
		 *
		 * @return
		 */
		public static Builder newBuilder ( final File directory ) {
			return new Builder ().rootDirectory ( directory );
		}

		/**
		 * @param directory
		 *
		 * @return
		 */
		public Builder rootDirectory ( final File directory ) {
			_directory = directory;
			return this;
		}

		/**
		 * @param partition
		 *
		 * @return
		 */
		public Builder partitions ( final int partition ) {
			_parts = partition;
			return this;
		}

		/**
		 * @return
		 */
		public WebCache build () {
			return new WebCache ( this );
		}

	}

}
